#!/usr/bin/env python

import os
import shutil
from argparse import ArgumentParser
from moho import moho_data_dir
from moho.core.logger import Logger


def create_project(args):
    Logger.setLevel('DEBUG')
    Logger.debug("moho_data_dir={}".format(moho_data_dir))

    new_project_name = args.project_name
    Logger.debug("project_name={}".format(new_project_name))

    new_project_path = os.path.join(os.curdir, new_project_name)
    Logger.debug("project_path={}".format(new_project_path))
    os.mkdir(new_project_path)

    new_project_type = args.project_type
    Logger.debug("project_type={}".format(new_project_type))

    if new_project_type == 'bootstrap':
        shutil.copyfile(os.path.join(moho_data_dir, 'starterapp', 'bsapp.py'),
                        os.path.join(new_project_path, 'app.py'))
    elif new_project_type == 'w3css':
        raise NotImplementedError()
    else:
        shutil.copyfile(os.path.join(moho_data_dir, 'starterapp', 'app.py'),
                        os.path.join(new_project_path, 'app.py'))

    with open(os.path.join(new_project_path, '__init__.py'), 'w'):
        pass

    shutil.copytree(os.path.join(moho_data_dir, 'static'), os.path.join(
        new_project_name, 'data', 'static'))
    shutil.copytree(os.path.join(moho_data_dir, 'templates'), os.path.join(
        new_project_name, 'data', 'templates'))

    Logger.info("Empty project is created. you may 'cd {}' and run 'python app.py'".format(
        new_project_path))


if __name__ == "__main__":

    arg_parser = ArgumentParser()
    arg_parser.add_argument('project', choices=['project'])
    arg_parser.add_argument('project_name')
    arg_parser.add_argument('--type', '-t', dest='project_type',
                            choices=['native', 'bootstrap', 'w3css'], default='native')
    args = arg_parser.parse_args()
    create_project(args)
