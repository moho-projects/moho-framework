"""
Textarea
========


"""

from moho.core.properties import NumericProperty, StringProperty
from moho.core.attributes import ClassAttribute
from moho.uix.bootstrap.input import InputField
from moho.uix.html5.textarea import TextAreaBase
from moho.uix.bootstrap.element import BootstrapElement

    
class TextArea(TextAreaBase, BootstrapElement):
    
    tagname = "textarea"
    
    feedback_style = ClassAttribute(
        None, options=["valid", "invalid"],
        values=["is-valid", "is-invalid"])
    
            
class BaseTextEdit(InputField):
    
    rows = NumericProperty(3)
    
    cols = NumericProperty(50)
    
    
class TextEdit(BaseTextEdit):
    pass


class FloatingTextEdit(BaseTextEdit):
    pass

