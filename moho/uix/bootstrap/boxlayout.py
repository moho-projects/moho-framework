"""
BoxLayout
======================

"""

from moho.core.properties import AliasProperty, OptionProperty
from moho.core.properties import BoundedNumericProperty
from moho.core.attributes import ClassAttribute
from moho.uix.bootstrap.element import BootstrapElement
from moho.uix.bootstrap.gridlayout import Row, Col


class BoxLayout(BootstrapElement):

    tagname = "div"

    display = ClassAttribute(
        "flex",
        options=["flex", "none"],
        values=["d-flex", "d-none"],
    )

    direction = ClassAttribute(
        "row",
        options=["row", "row_reverse", "column", "column_reverse"],
        values=["flex-row", "flex-row-reverse",
                "flex-column", "flex-column-reverse"]
    )

    justify_content = ClassAttribute(
        None,
        options=["start", "end", "center", "between", "around"],
        values=["justify-content-start",
                "justify-content-end", "justify-content-center",
                "justify-content-between", "justify-content-around"]
    )

    align_items = ClassAttribute(
        None,
        options=["start", "end", "center", "baseline", "stretch"],
        values=["align-items-start",
                "align-items-end", "align-items-center",
                "align-items-baseline",
                "align-items-stretch"]
    )

    align_content = ClassAttribute(
        None,
        options=["start", "end", "center", "around", "stretch"],
        values=["align-content-start", "align-content-end",
                "align-content-center", "align-content-around",
                "align-content-stretch"],

    )

    wrap = ClassAttribute(
        None,
        options=["nowrap", "wrap", "wrap reverse"],
        values=["flex-nowrap", "flex-wrap", "flex-wrap-reverse"],

    )

    align_self = ClassAttribute(
        None,
        options=["start", "end", "center", "baseline", "stretch"],
        values=["align-self-start", "align-self-end",
                "align-self-center", "align-self-baseline",
                "align-self-stretch"],

    )

    hidden = ClassAttribute(
        None,
        options=["on", "off"],
        values=["invisible", "visible"],
        allownone=True
    )

    def on_hidden(self, obj, val):
        if val == "on":
            self.display = "none"
        else:
            self.display = "flex"


class HBoxLayout(BoxLayout):
    pass


class VBoxLayout(BoxLayout):
    pass
