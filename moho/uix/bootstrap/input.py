"""
Input
=====

Set of classes representing various types of html inputs

"""

from moho.core.properties import ObjectProperty, StringProperty, \
BooleanProperty, OptionProperty
from moho.core.attributes import ElementAttribute, ClassAttribute
from moho.uix.element import NoContentHtmlElement
from moho.uix.html5.input import InputBase
from moho.uix.bootstrap.element import BootstrapElement


class InputFeedback(BootstrapElement):
    
    tagname = "div"
    
    text = StringProperty('')
    
    feedback_style = ClassAttribute(
        None, options=["valid", "invalid"],
        values=["valid-feedback", "invalid-feedback"])
    
    
class InputHint(BootstrapElement):
    
    tagname = "small"
    
    text = StringProperty('') 


class InputLabel(BootstrapElement):
    """
    Label class, represents html label element
    """
    
    tagname = "label"
    
    text = StringProperty('')

    forattr = ElementAttribute(None)


class Input(InputBase, NoContentHtmlElement):
    
    tagname = "input"
    
    feedback_style = ClassAttribute(
        None, options=["valid", "invalid"],
        values=["is-valid", "is-invalid"])

    size_cls = ClassAttribute(
        None, options=["large", "small"],
        values=["form-control-lg", "form-control-sm"])


class InputField(BootstrapElement):
    
    tagname = 'div'
    
    input_element = ObjectProperty(None)

    feedback_element = ObjectProperty(None)

    placeholder = StringProperty('')

    label = StringProperty('')
    
    readonly = BooleanProperty(False)
    
    hint = StringProperty('')

    disabled = BooleanProperty(False)

    value = StringProperty('')

    feedback = StringProperty('')

    feedback_style = OptionProperty(
        None, options=["valid", "invalid"])

