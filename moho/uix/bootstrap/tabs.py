"""
Tabs
======================

"""
from moho.core.properties import (
    OptionProperty, ObjectProperty, BooleanProperty,
    StringProperty)

from moho.uix.html5.hyperlink import HyperLinkBase
from moho.core.attributes import ElementAttribute, ClassAttribute, ClassBooleanAttribute, \
    ElementLogicalAttribute
from moho.uix.bootstrap.boxlayout import VBoxLayout, BoxLayout
from moho.uix.bootstrap.element import BootstrapElement


class TabPanel(VBoxLayout):
    
    role = ElementAttribute(None, options=["tabpanel"])
    
    active = ClassBooleanAttribute(False)
    
    disabled = BooleanProperty(False)
    
    show = ClassBooleanAttribute(False)
    
    aria_labelledby = ElementAttribute(None)
    
    name = StringProperty("")
    
    link = ObjectProperty(None)
    
    def __init__(self, *args, **kwargs):
        super(TabPanel, self).__init__(*args, **kwargs)
        self._create_link()
        
    def _create_link(self):
        link = TabNavLink(
            href="#",
            aria_controls=self.tagid,
            aria_selected=self.active,
            content=self.name,
            onclick=True)
        
        link.bind(on_remote_click=self.on_link_click)
        
        self.aria_labelledby = link.tagid
        
        self.bind(active=link.setter("active"))
        self.bind(name=link.setter("content"))
        self.bind(disabled=link.setter("disabled"))
        self.bind(active=link.setter("aria_selected"))
        
        self.link = link
        
    def on_link_click(self, obj, **kwargs):
        self.active = True
 
 
class TabNavLink(BootstrapElement, HyperLinkBase):
    
    tagname = "a"
    
    role = ElementAttribute(None, options=["tab"])

    active = ClassBooleanAttribute(False)

    disabled = ClassBooleanAttribute(False)

    aria_selected = ElementLogicalAttribute(False)
    
    aria_controls = ElementAttribute(False)
    
    def on_remote_click(self, *args, **kwargs):
        self.area_selected = True
        self.active = True
 
    
class TabNav(BootstrapElement):
    
    tagname = "div"

    navtype = ClassAttribute("tabs", options=["tabs", "pills"],
                             values=["nav-tabs", "nav-pills"])
    
    role = ElementAttribute(None, options=["tablist"])
    
    aria_orientation = ElementAttribute(
        "horizontal", options=["vertical", "horizontal"])
    
    orientation = ClassAttribute(
        "horizontal",
        options=["horizontal", "vertical"],
        values=["flex-row", "flex-column"]
    )
                     

class TabContent(BootstrapElement):
    
    tagname = "div"
    
    navigation = ObjectProperty(None, allownone=True)
        
    def add_element(self, element, index=0):
        if isinstance(element, TabPanel):
            element.bind(active=self.on_panel_activated)
            if self.navigation is not None:
                self.navigation.add_element(element.link, index=index)
        super(TabContent, self).add_element(element, index=index)
        
    def remove_element(self, element):
        if isinstance(element, TabPanel):
            element.unbind(active=self.on_panel_activated)
            if self.navigation is not None:
                self.navigation.remove_element(element.link)
        super(TabContent, self).remove_element(element)
        
    def on_navigation(self, obj, nav):
        if nav is None:
            return
        for ch in self.children:
            if isinstance(ch, TabPanel) is not None:
                nav.add_element(ch.link)
                
    def on_panel_activated(self, obj, active):
        self._unbind_panels()
        if active:
            for ch in self.children:
                if ch is not obj:
                    ch.active = False
        self._bind_panels()
        
    def _bind_panels(self):
        for ch in self.children:
            if isinstance(ch, TabPanel):
                ch.bind(active=self.on_panel_activated)
    
    def _unbind_panels(self):
        for ch in self.children:
            if isinstance(ch, TabPanel):
                ch.unbind(active=self.on_panel_activated)
                

class Tab(BoxLayout):
    
    navigation = ObjectProperty(None)
    container = ObjectProperty(None)
    
    orientation = OptionProperty(
        "horizontal",
        options=["horizontal", "vertical"]
    )
    
    def add_element(self, element, index=0):
        if isinstance(element, TabContent):
            element.navigation = self.navigation
            self.bind(navigation=element.setter("navigation"))
            self.container = element
        if isinstance(element, TabNav):
            element.orientation = self.orientation
            self.bind(orientation=element.setter("orientation"))
            self.navigation = element
            
        super(Tab, self).add_element(element, index=index)
        
    def remove_element(self, element):
        if not isinstance(element, TabContent):
            self.container = None
            self.unbind(navigation=element.setter("navigation"))
        if isinstance(element, TabNav):
            self.unbind(orientation=element.setter("orientation"))
            self.navigation = None
        super(Tab, self).remove_element(element)
            
