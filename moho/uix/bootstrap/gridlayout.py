"""
BoxLayout
======================

"""
from moho.core.properties import AliasProperty, OptionProperty
from moho.core.properties import BoundedNumericProperty
from moho.core.attributes import ClassAttribute
from moho.uix.bootstrap.element import BootstrapElement

MAXCOLS = 12


class Col(BootstrapElement):

    tagname = "div"

    size_xs = ClassAttribute(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"],
        values=["col-xs-1", "col-xs-2", "col-xs-3", "col-xs-4", "col-xs-5",
                "col-xs-6", "col-xs-7", "col-xs-8", "col-xs-9", "col-xs-10",
                "col-xs-11", "col-xs-12"]
    )

    size_sm = ClassAttribute(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"],
        values=["col-sm-1", "col-sm-2", "col-sm-3", "col-sm-4", "col-sm-5",
                "col-sm-6", "col-sm-7", "col-sm-8", "col-sm-9", "col-sm-10",
                "col-sm-11", "col-sm-12"]
    )

    size_md = ClassAttribute(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"],
        values=["col-md-1", "col-md-2", "col-md-3", "col-md-4", "col-md-5",
                "col-md-6", "col-md-7", "col-md-8", "col-md-9", "col-md-10",
                "col-md-11", "col-md-12"]
    )

    size_lg = ClassAttribute(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"],
        values=["col-lg-1", "col-lg-2", "col-lg-3", "col-lg-4", "col-lg-5",
                "col-lg-6", "col-lg-7", "col-lg-8", "col-lg-9", "col-lg-10",
                "col-lg-11", "col-lg-12"]
    )

    size_xl = ClassAttribute(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"],
        values=["col-xl-1", "col-xl-2", "col-xl-3", "col-xl-4", "col-xl-5",
                "col-xl-6", "col-xl-7", "col-xl-8", "col-xl-9", "col-xl-10",
                "col-xl-11", "col-xl-12"]
    )

    size_xxl = ClassAttribute(
        None,
        options=["1", "2", "3", "4", "5", "6", "7", "8",
                 "9", "10", "11", "12"],
        values=["col-xxl-1", "col-xxl-2", "col-xxl-3", "col-xxl-4",
                "col-xxl-5",
                "col-xxl-6", "col-xxl-7", "col-xxl-8", "col-xxl-9",
                "col-xxl-10",
                "col-xxl-11", "col-xxl-12"]
    )

    def add_element(self, element, index=0):
        BootstrapElement.add_element(self, element, index=index)
        if isinstance(element, BootstrapElement):
            self.bind_size_hint(element)

    def remove_element(self, element, index=0):
        BootstrapElement.remove_element(self, element, index=index)
        if isinstance(element, BootstrapElement):
            self.unbind_size_hint(element)

    def bind_size_hint(self, el):
        el.bind(size_hint_xs=self.setter("size_xs"),
                size_hint_sm=self.setter("size_sm"),
                size_hint_md=self.setter("size_md"),
                size_hint_xl=self.setter("size_xl"),
                size_hint_xxl=self.setter("size_xxl"))

    def unbind_size_hint(self, el):
        el.unbind(
            size_hint_xs=self.setter("size_xs"),
            size_hint_sm=self.setter("size_sm"),
            size_hint_md=self.setter("size_md"),
            size_hint_xl=self.setter("size_xl"),
            size_hint_xxl=self.setter("size_xxl"))

    def get_content(self):
        cnt = ""
        for ch in reversed(self.children):
            if isinstance(ch, GridLayout):
                cnt += ch.content
                continue
            cnt += ch.html
        cnt += self._content
        return cnt

    def set_content(self, cnt):
        self._content = str(cnt)
        return True

    # we do not bind to children
    content = AliasProperty(get_content, set_content)
    '''Return string containing html code for children of this element.'''


class Row(BootstrapElement):

    tagname = "div"

    gutters = ClassAttribute(
        "false",
        options=["true", "false"],
        values=[None, "row-no-gutters"]
    )

    __accepted_children_types__ = [Col]


class GridLayout(BootstrapElement):

    tagname = "div"

    cols = BoundedNumericProperty(1, min=1, max=MAXCOLS)

    size = ClassAttribute(
        "fluid",
        options=["default", "fluid", "sm", "md", "lg", "xl", "xxl"],
        values=["container", "container-fluid", "container-sm",
                "container-md", "container-lg", "container-xl",
                "container-xxl"]
    )

    gutters = OptionProperty(
        "true",
        options=["true", "false"],
    )

    def get_content(self):
        cnt = ""
        crow = Row(gutters=self.gutters)
        for num, ch in enumerate(reversed(self.children)):
            if isinstance(ch, Row):
                crow = ch
                cnt += crow.html
                continue
            if isinstance(ch, Col):
                col = ch
            else:
                col = Col(
                    size_xs=ch.size_hint_xs,
                    size_sm=ch.size_hint_sm,
                    size_md=ch.size_hint_md,
                    size_lg=ch.size_hint_lg,
                    size_xl=ch.size_hint_xl,
                    size_xxl=ch.size_hint_xxl
                )
                ch.parent = None
                col.add_element(ch)
            crow.add_element(col)
            if (num + 1) % self.cols == 0:
                cnt += crow.html
                crow = Row(gutters=self.gutters)
        cnt += self._content
        return cnt

    def set_content(self, cnt):
        self._content = str(cnt)
        return True

    # we do not bind to children
    content = AliasProperty(get_content, set_content)
    '''Return string containing html code for children of this element.'''

