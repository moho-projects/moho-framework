"""
Spinner
======================

"""
from moho.core.attributes import ClassAttribute
from moho.core.properties import StringProperty
from moho.uix.bootstrap.element import BootstrapElement


class BaseSpinner(BootstrapElement):
    
    tagname = "div"

    spinner_style = ClassAttribute("primary", options=[
        "primary",
        "secondary",
        "success",
        "info",
        "warning",
        "danger",
        "dark",
        "light"
        ],

        values=[
            "text-primary",
            "text-secondary",
            "text-success",
            "text-info",
            "text-warning",
            "text-danger",
            "text-dark",
            "text-light"
        ], allownone=True
    )
    
    html_tpl = StringProperty(
        '<{{obj.tagname}} id="{{obj.tagid}}" {% if obj.cls %} class="{{obj.cls}}" {% end %}'
        '{% if obj.style %} style="{{obj.style}}" {% end %} {% raw obj.attributes %} role="status">'
        '<span class="sr-only">Loading...</span>'
        '</{{obj.tagname}}>'
    )  
    

class Spinner(BaseSpinner):

    spinner_size = ClassAttribute(None, options=["large", "small"],
                                 values=["", "spinner-border-sm"])


class GrowSpinner(BootstrapElement):
    
    tagname = "div"

    spinner_size = ClassAttribute(None, options=["large", "small"],
                                 values=["", "spinner-grow-sm"])
    
