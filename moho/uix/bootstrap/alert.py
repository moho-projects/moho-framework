"""
Alert
======================

"""

from moho.core.attributes import (ClassAttribute)
from moho.core.properties import (
    StringProperty)
from moho.uix.bootstrap.element import BootstrapElement


class Alert(BootstrapElement):

    tagname = "div"
    
    text = StringProperty('')
    
    alert_style = ClassAttribute("primary", options=[
        "primary", "secondary", "success", "info",
        "warning", "danger", "default"],

        values=["alert-primary", "alert-secondary", "alert-success", "alert-info",
                "alert-warning", "alert-danger", "alert-default"]
    )
    
    html_tpl = StringProperty("""<{{obj.tagname}} id='{{obj.tagid}}' {% if obj.cls %} class='{{obj.cls}}'
            {% end %} {% if obj.style %} style='{{obj.style}}' {% end %} {% raw obj.attributes %} role="alert" >
            {%raw obj.content%} <button type='button' class='btn-close' data-bs-dismiss='alert'
            aria-label='Close'></button> </{{obj.tagname}}>"""
            )
