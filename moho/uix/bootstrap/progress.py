"""
Progress
======================

"""
from moho.core.properties import (
    BooleanProperty, ObjectProperty,
    BoundedNumericProperty, OptionProperty)

from moho.core.attributes import ElementAttribute, ElementNumericAttribute, ClassAttribute, \
    ClassBooleanAttribute
from moho.uix.bootstrap.element import BootstrapElement


class ProgressBar(BootstrapElement):
    
    tagname = "div"
    
    role = ElementAttribute("progressbar", options=['progressbar'])
    aria_valuenow = ElementNumericAttribute(0)
    aria_valuemin = ElementNumericAttribute(0)
    aria_valuemax = ElementNumericAttribute(100)

    striped = ClassBooleanAttribute(
        False, attrname="progress-bar-striped")

    animated = ClassBooleanAttribute(
        False, attrname="progress-bar-animated")

    type = ClassAttribute(
        "default", options=["default", "success", "info", "warning", "danger"],
        values=["", "bg-success", "bg-info", "bg-warning", "bg-danger"])


class Progress(BootstrapElement):
    
    tagname = "div"
    
    progress_bar = ObjectProperty(None)
    value = BoundedNumericProperty(
        0, min=0, max=100,
        errorhandler=lambda x: 100 if x > 100 else 0)
    labeled = BooleanProperty(False)
    striped = BooleanProperty(False)
    animated = BooleanProperty(False)
    type = OptionProperty(
        "default", options=["default", "success", "info", "warning", "danger"])

    __accepted_children_types__ = [ProgressBar]

