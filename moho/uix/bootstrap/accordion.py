"""
Accordion
======================




"""

from moho.core.properties import (
    BooleanProperty, ObjectProperty, StringProperty)
from moho.core.attributes import ElementAttribute, ClassBooleanAttribute, ElementLogicalAttribute
from moho.uix.bootstrap.button import Button
from moho.uix.bootstrap.element import BootstrapElement


class AccordionHeadButton(Button):

    data_bs_toggle = ElementAttribute("collapse", options=["collapse"])

    data_bs_target = ElementAttribute(None)

    aria_expanded = ElementLogicalAttribute(False)

    aria_controls = ElementAttribute(None)

    collapsed = ClassBooleanAttribute(False)


class AccordionItemBody(BootstrapElement):
    tagname = 'div'
    show = ClassBooleanAttribute(False, remote=True)
    

class AccordionItemBodyContainer(BootstrapElement):
    
    tagname = "div"

    aria_labelledby = ElementAttribute(None)

    data_bs_parent = ElementAttribute(None)

    body = ObjectProperty(None)
    
    show = ClassBooleanAttribute(False, remote=True)
    
    def add_element(self, element, index=0):
        if isinstance(element, AccordionItemBody):
            super(AccordionItemBodyContainer, self).add_element(element, index)
        else:
            self.body.add_element(element, index)


class AccordionItemHeader(BootstrapElement):
    
    tagname = "div"
    
    text = StringProperty('')
    
    container_tagid = StringProperty('')
    
    collapsed = BooleanProperty(False)


class AccordionItem(BootstrapElement):
    
    tagname = "div"
    
    body_container = ObjectProperty(None)
    
    header = StringProperty('')
    
    collapsed = BooleanProperty(False)
    
    show = ClassBooleanAttribute(False, remote=True)
    
    def add_element(self, element, index=0):
        if not isinstance(element, (
                AccordionItemHeader,
                AccordionItemBodyContainer)):
            self.body_container.add_element(element, index)
        else:
            super(AccordionItem, self).add_element(
                element, index=index)


class Accordion(BootstrapElement):
    
    tagname = "div"
    
    __accepted_children_types__ = [AccordionItem]
    
