"""
Dropdown
======================

"""

from moho.core.properties import (
    StringProperty,
    ObjectProperty, OptionProperty, ListProperty, BooleanProperty)
from moho.uix.bootstrap.button import Button
from moho.core.attributes import ElementAttribute, ClassBooleanAttribute, \
    ClassAttribute, ElementLogicalAttribute
from moho.uix.bootstrap.element import BootstrapElement


class DropdownButton(Button):

    type = ElementAttribute("button", options=["button"])

    data_toggle = ElementAttribute("dropdown", options=["dropdown"])

    aria_expanded = ElementLogicalAttribute(False, remote=True)

    aria_haspopup = ElementLogicalAttribute(False, remote=True)



class DropdownMenu(BootstrapElement):

    tagname = "div"

    role = ElementAttribute("menu", options=["menu"])

    show = ClassBooleanAttribute(False)

    alignment = ClassAttribute(
        None, options=["left", "right"],
        values=["dropdown-menu-left", "dropdown-menu-right"])

    def on_remote_click(self, *args, **kwargs):
        self.parent.dispatch("on_remote_click", self.text)



class DropdownItem(BootstrapElement):

    tagname = "a"

    href = ElementAttribute('#', remote=True)
    """
    Specifies the URL of the page the link goes to
    """

    text = StringProperty("")

    disabled = ClassBooleanAttribute(False) 

    def on_remote_click(self, *args, **kwargs):
        self.parent.dispatch("on_remote_click", self.text)


class DropdownDivider(BootstrapElement):
    
    tagname = "div"


class DropdownHeader(BootstrapElement):
    
    tagname = "div"
    
    text = StringProperty('')


class Dropdown(BootstrapElement):
    
    tagname = "div"
    
    disabled = BooleanProperty(False)

    show = ClassBooleanAttribute(False)

    type = ClassAttribute("dropdown", options=["dropdown", "dropup"])

    alignment = OptionProperty(
        None, options=["left", "right"], allownone=True)

    menu = ObjectProperty(None)

    text = StringProperty('')

    btn_size = OptionProperty(
        None, options=["large", "small"], allownone=True)

    values = ListProperty([])
    
    btn_style = OptionProperty("primary", options=[
        "primary", "secondary", "success", "info",
        "warning", "danger", "link", "default"]
    )
    
    mt = ClassAttribute("0",
                                options=["0", "1", "2", "3", "4", "5"],
                                values=["mt-0", "mt-1",
                                        "mt-2", "mt-3",
                                        "mt-4", "mt-5"])
    
    mr = ClassAttribute("0",
                                options=["0", "1", "2", "3", "4", "5"],
                                values=["mr-0", "mr-1",
                                        "mr-2", "mr-3",
                                        "mr-4", "mr-5"])
    
    def __init__(self, **kwargs):
        super(Dropdown, self).__init__(**kwargs)
        self.register_event_type("on_remote_click")
        
    def on_remote_click(self, *args, **kwargs):
        pass
    
    def add_element(self, element, index=0):
        if isinstance(element, (
                DropdownItem, DropdownDivider, DropdownHeader)):
            self.menu.add_element(element, index)
        else:
            super(Dropdown, self).add_element(element, index)

    def on_values(self, obj, val):
        self.menu.clear_elements()
        for item in self.values:
            if item == "--":
                self.add_element(DropdownDivider())
            elif item.startswith("<") and item.endswith(">"):
                self.add_element(DropdownHeader(txt=item[1:-2]))
            else:
                self.add_element(DropdownItem(text=item))
