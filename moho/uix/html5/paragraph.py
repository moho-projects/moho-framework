"""
Paragraph
=========

"""

from moho.uix.element import HtmlElement


class Paragraph(HtmlElement):
    """
    Paragraph class, represents html <p> element
    """

    tagname = "p"


class SmallText(HtmlElement):
    """
    SmallText class, represents html <small> element
    """

    tagname = "small"


class Span(HtmlElement):
    """
    Span class, represent html <span> element
    """
    tagname = "span"