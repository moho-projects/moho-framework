"""
Tables
======

"""

from moho.uix.element import HtmlElement
from moho.core.attributes import ElementAttribute, ElementNumericAttribute, \
ElementListAttribute


class TableHeaderCell(HtmlElement):
    tagname = "th"
    abbr = ElementAttribute(None, remote=True)
    colspan = ElementNumericAttribute(None, remote=True)
    headers = ElementListAttribute(None, remote=True)
    rowspan = ElementNumericAttribute(None, remote=True)
    scope = ElementAttribute(None, options=["col", "colgroup", "row", "rowgroup"], remote=True)
    
    
class TableDataCell(HtmlElement):
    tagname = "td"
    colspan = ElementNumericAttribute(None, remote=True)
    headers = ElementListAttribute(None, remote=True)
    rowspan = ElementNumericAttribute(None, remote=True)
    
      
class TableRow(HtmlElement):
    tagname = "tr"
    
    __accepted_children_types__ = [TableHeaderCell, TableDataCell]
    
        
        
class TableHeader(HtmlElement):
    tagname = "thead"
    
    __accepted_children_types__ = [TableRow]
    
       

class TableFooter(HtmlElement):
    tagname = "tfoot"
    
    __accepted_children_types__ = [TableRow]
    

class TableBody(HtmlElement):
    tagname = "tbody"
    
    __accepted_children_types__ = [TableRow]
    

class Table(HtmlElement):
    """
    Table class, represents basic table element
    """
    tagname = "table"
    
    __accepted_children_types__ = [TableBody, TableFooter, TableHeader]
    