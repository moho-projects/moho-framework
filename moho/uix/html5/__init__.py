"""
HTML5 UIX classes
=================

HTML5 elements are low-level components and do not come with javascript libraries or CSS style files.
Thus, implementing a functional GUI application using pure HTML5 elements requires quite a bit of work, especially for styling.
These classes exist and are used as base classes or applied when an instance of a pure HTML element is sufficient. 

"""


