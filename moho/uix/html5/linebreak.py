"""
Line Break
==========

"""

from moho.uix.element import NoContentHtmlElement


class LineBreak(NoContentHtmlElement):
    """
    LineBreak class, represents html <br/> element
    """

    tagname = "br"
