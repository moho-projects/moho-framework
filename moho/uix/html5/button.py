"""
Button
======

The `Button` class defines a clickable button element with tag `<button>` (not the `input` element).
Inside the `Button` element you can put text and instances of other elements.  
 

"""

from moho.uix.element import HtmlElement
from moho.core.attributes import ElementAttribute, ElementBooleanAttribute


class ButtonBase:

    autofocus = ElementBooleanAttribute(False)
    """
    Specifies that a button should automatically get focus when the page loads.
    """

    disabled = ElementBooleanAttribute(False, remote=True)
    """
    Specifies that a button should be disabled.
    """

    form = ElementAttribute(None)
    """
    Specifies which form the button belongs to.
    """

    name = ElementAttribute(None)
    """
    Specifies a name for the button (used by forms).
    """

    type = ElementAttribute(None, options=["button", "reset"])
    """
    Specifies the type of button.
    """


class Button (ButtonBase, HtmlElement):
    """
    Represents basic button html element.
    """
    
    tagname = "button"

    autofocus = ElementBooleanAttribute(False)
    """
    Specifies that a button should automatically get focus when the page loads.
    """

    disabled = ElementBooleanAttribute(False, remote=True)
    """
    Specifies that a button should be disabled.
    """

    form = ElementAttribute(None)
    """
    Specifies which form the button belongs to.
    """

    name = ElementAttribute(None)
    """
    Specifies a name for the button (used by forms).
    """

    type = ElementAttribute(None, options=["button", "reset"])
    """
    Specifies the type of button.
    """