
from moho.uix.x3d.appearance import X3dAppearance
from moho.uix.x3d.element import X3dElement
from moho.core.attributes import (
    ElementVariableListAttribute, ElementLogicalAttribute,
    ElementNumericAttribute, ElementAttribute, ElementListAttribute)


class X3dGeometry(X3dElement):

    ccw = ElementLogicalAttribute(None, remote=True)

    lit = ElementLogicalAttribute(None, remote=True)

    solid = ElementLogicalAttribute(None, remote=True)


class X3dSpatialGeometry(X3dGeometry):
    pass


class X3dShape(X3dElement):

    tagname = "Shape"

    bbox_center = ElementVariableListAttribute(
        None, maxlength=3, attrname="bboxCenter", remote=True
    )

    bbox_size = ElementVariableListAttribute(
        None, maxlength=3, attrname="bboxsize", remote=True
    )

    render = ElementLogicalAttribute(None, remote=True)

    ispickable = ElementLogicalAttribute(None, remote=True)

    __accepted_children_types__ = [X3dAppearance, X3dGeometry]


class X3dBox(X3dSpatialGeometry):

    tagname = "Box"

    size = ElementListAttribute(None, length=3, remote=True)


class X3dCone(X3dSpatialGeometry):

    tagname = "Cone"

    bottom = ElementLogicalAttribute(None, remote=True)

    bottom_radius = ElementNumericAttribute(None, attrname="bottomRadius", remote=True)

    height = ElementNumericAttribute(None, remote=True)

    side = ElementLogicalAttribute(None, remote=True)

    subdivision = ElementNumericAttribute(None, remote=True)

    top = ElementLogicalAttribute(None, remote=True)

    top_radius = ElementNumericAttribute(None, attrname="topRadius", remote=True)


class X3dCylinder(X3dSpatialGeometry):

    tagname = "Cylinder"

    bottom = ElementLogicalAttribute(None, remote=True)

    radius = ElementNumericAttribute(None, remote=True)

    height = ElementNumericAttribute(None, remote=True)

    side = ElementLogicalAttribute(None, remote=True)

    subdivision = ElementNumericAttribute(None, remote=True)

    top = ElementLogicalAttribute(None, remote=True)


class X3dDish(X3dSpatialGeometry):

    tagname = "Dish"

    bottom = ElementLogicalAttribute(None, remote=True)

    diameter = ElementNumericAttribute(None, remote=True)

    radius = ElementNumericAttribute(None, remote=True)

    height = ElementNumericAttribute(None, remote=True)

    side = ElementLogicalAttribute(None, remote=True)

    subdivision = ElementVariableListAttribute(None, maxlength=2, remote=True)


class X3dExternalGeometry(X3dSpatialGeometry):

    tagname = "ExternalGeometry"

    url = ElementAttribute(None)


class X3dNozzle(X3dSpatialGeometry):

    tagname = "Nozzle"

    height = ElementNumericAttribute(None, remote=True)

    inner_radius = ElementNumericAttribute(None, attrname="innerRadius", remote=True)

    nozzle_height = ElementNumericAttribute(None, attrname="nozzleHeight", remote=True)

    nozzle_radius = ElementNumericAttribute(None, attrname="nozzleRadius", remote=True)

    outer_radius = ElementNumericAttribute(None, attrname="outerRadius", remote=True)

    subdivision = ElementNumericAttribute(None, remote=True)


class X3dPyramid(X3dSpatialGeometry):

    tagname = "Pyramid"

    height = ElementNumericAttribute(None, remote=True)

    xbottom = ElementNumericAttribute(None, remote=True)

    ybottom = ElementNumericAttribute(None, remote=True)

    xtop = ElementNumericAttribute(None, remote=True)

    ytop = ElementNumericAttribute(None, remote=True)

    xoff = ElementNumericAttribute(None, remote=True)

    yoff = ElementNumericAttribute(None, remote=True)


class X3dRectTorus(X3dSpatialGeometry):

    tagname = "RectangularTorus"

    angle = ElementNumericAttribute(None, remote=True)

    caps = ElementLogicalAttribute(None, remote=True)

    height = ElementNumericAttribute(None, remote=True)

    inner_radius = ElementNumericAttribute(None, attrname="innerRadius", remote=True)

    outer_radius = ElementNumericAttribute(None, attrname="outerRadius", remote=True)

    subdivision = ElementNumericAttribute(None, remote=True)


class X3dSlopedCylinder(X3dSpatialGeometry):

    tagname = "SlopedCylinder"

    height = ElementNumericAttribute(None, remote=True)

    bottom = ElementNumericAttribute(None, remote=True)

    top = ElementNumericAttribute(None, remote=True)

    radius = ElementNumericAttribute(None, remote=True)

    subdivision = ElementNumericAttribute(None, remote=True)

    xbshear = ElementNumericAttribute(None, remote=True)

    xtshear = ElementNumericAttribute(None, remote=True)

    ybshear = ElementNumericAttribute(None, remote=True)

    ytshear = ElementNumericAttribute(None, remote=True)


class X3dSnout(X3dSpatialGeometry):

    tagname = "Snout"

    bottom = ElementNumericAttribute(None, remote=True)

    dbottom = ElementNumericAttribute(None, remote=True)

    dtop = ElementNumericAttribute(None, remote=True)

    height = ElementNumericAttribute(None, remote=True)

    subdivision = ElementNumericAttribute(None, remote=True)

    top = ElementNumericAttribute(None, remote=True)

    xoff = ElementNumericAttribute(None, remote=True)

    yoff = ElementNumericAttribute(None, remote=True)


class X3dSphere(X3dSpatialGeometry):

    tagname = "Sphere"

    radius = ElementNumericAttribute(None, remote=True)

    subdivision = ElementVariableListAttribute(None, maxlength=2, remote=True)


class X3dTorus(X3dSpatialGeometry):

    tagname = "Torus"

    angle = ElementNumericAttribute(None, remote=True)

    caps = ElementLogicalAttribute(None, remote=True)

    inner_radius = ElementNumericAttribute(None, attrname="innerRadius", remote=True)

    outer_radius = ElementNumericAttribute(None, attrname="outerRadius", remote=True)

    subdivision = ElementNumericAttribute(None, remote=True)


class X3dExtrusion(X3dSpatialGeometry):

    tagname = "Extrusion"

    beginCap = ElementLogicalAttribute(None, remote=True)

    convex = ElementLogicalAttribute(None, remote=True)

    crease_angle = ElementNumericAttribute(None, attrname="creaseAngle", remote=True)

    cross_section = ElementListAttribute(None, attrname="crossSection", remote=True)

    endCap = ElementLogicalAttribute(None, remote=True)

    height = ElementNumericAttribute(None, remote=True)

    orientation = ElementListAttribute(None, remote=True)

    scale = ElementListAttribute(None, remote=True)

    spine = ElementListAttribute(None, remote=True)

    angle = ElementNumericAttribute(None, remote=True)

    caps = ElementLogicalAttribute(None, remote=True)

    inner_radius = ElementNumericAttribute(None, attrname="innerRadius", remote=True)

    outer_radius = ElementNumericAttribute(None, attrname="outerRadius", remote=True)

    subdivision = ElementNumericAttribute(None, remote=True)
