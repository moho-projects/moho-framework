
from moho.uix.x3d.element import X3dElement
from moho.core.attributes import ElementNumericAttribute, ElementAttribute,\
    ElementLogicalAttribute, ElementVariableListAttribute, ElementColorAttribute


class X3dBlendMode(X3dElement):

    tagname = "BlendMode"

    color = ElementVariableListAttribute(None, maxlength=3, remote=True)

    transparency = ElementNumericAttribute(0, remote=True)


class X3dLineProperties(X3dElement):

    tagname = "LineProperties"

    applied = ElementLogicalAttribute(True, remote=True)

    linetype = ElementNumericAttribute(1, remote=True)

    line_width_sfactor = ElementNumericAttribute(
        1, attrname="linewidthScaleFactor", remote=True)


class X3dMaterial(X3dElement):

    tagname = "Material"

    ambient_intensity = ElementNumericAttribute(
        None, attrname="ambientIntensity", remote=True)

    diffuse_color = ElementColorAttribute(
        None, attrname="diffuseColor", remote=True)

    emissive_color = ElementColorAttribute(
        None, attrname="emissiveColor", remote=True)

    shininess = ElementNumericAttribute(None, remote=True)

    specular_color = ElementColorAttribute(
        None, attrname="specularColor", remote=True)

    transparency = ElementNumericAttribute(None, remote=True)


class X3dTexture(X3dElement):

    tagname = "Texture"

    repeat_s = ElementLogicalAttribute(None, attrname="repeatS", remote=True)

    repeat_t = ElementLogicalAttribute(None, attrname="repeatT", remote=True)

    scale = ElementLogicalAttribute(None, remote=True)

    url = ElementAttribute(None, remote=True)


class X3dTextureTransform(X3dElement):

    tagname = "TextureTransform"

    center = ElementVariableListAttribute(None, length=2, remote=True)

    rotation = ElementNumericAttribute(None, remote=True)

    scale = ElementVariableListAttribute(None, length=2, remote=True)

    translate = ElementVariableListAttribute(None, length=2, remote=True)


class X3dAppearance(X3dElement):

    tagname = "Appearance"

    alpha_clip_threshold = ElementNumericAttribute(None, remote=True)

    __accepted_children_types = [
        X3dBlendMode, X3dLineProperties, X3dMaterial, X3dTexture, X3dTextureTransform]
