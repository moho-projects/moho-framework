
__all__ = ('_Attribute', '_NumericAttribute', '_BoundedNumericAttribute', '_BooleanAttribute',
           '_VariableListAttribute', '_ColorAttribute', '_GenericEventAttribute')

from moho.core.properties cimport (Property,
           NumericProperty, ListProperty, BooleanProperty,
           BoundedNumericProperty,
           VariableListProperty, PropertyStorage)

from moho.core.properties import ObservableList, CSS_LENGTH_UNITS

from moho.core.compat import string_types
from moho.core.utils import get_color_from_hex_255



cdef class _Attribute(Property):
    '''General Html attribute class with a string value.

    :Parameters:
        `defaultvalue`: string, defaults to ''
        Specifies the default value of the attribute.
        `attrtype`: string, attribute type - 'style', 'class', 'element'
        `remote`: boolean, relevant for element attributes only, class and style attributes
        are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `options`: list of strings, other possible values which are accepted by the attribute instance.
        For example some numeric attributes can accept also string values like `inherit`
        'values': list of values relevant for options. This list contain actual values which are sent to frontend when
        the property value is set to one of optional values. If `values` list is not set, it is equal to `options`
    '''

    def __cinit__(self):
        self._attrtype = ''
        self._remote = 0
        self.options = []
        self.values = {}
        self._attrname = ''

    def __init__(self, defaultvalue='', **kw):
        self._attrtype = kw.get('attrtype', '')
        self._attrname = kw.get('attrname', '')
        self._remote = kw.get('remote', 0)
        self.options = list(kw.get('options', []))
        if self.options:
            values = list(kw.get('values', []))
            if not values:
                values = self.options[:]
            if len(self.options) != len(values):
                raise AttributeError(
                    "length mismatch between options and values lists")
            self.values = dict(zip(self.options, values))
        kw['allownone'] = True
        super(_Attribute, self).__init__(defaultvalue, **kw)

    cdef init_storage(self, EventDispatcher obj, PropertyStorage storage):
        Property.init_storage(self, obj, storage)
        storage.options = self.options[:]

    cdef check(self, EventDispatcher obj, value):
        if Property.check(self, obj, value):
            return True
        if not isinstance(value, string_types):
            raise ValueError('%s.%s accept only str' % (
                obj.__class__.__name__,
                self.name))
        cdef PropertyStorage ps = obj.__storage[self._name]
        if ps.options and value not in ps.options:
            raise ValueError('%s.%s is set to an invalid option %r. '
                             'Must be one of: %s' % (
                                 obj.__class__.__name__,
                                 self.name,
                                 value, ps.options))

    cdef compare_value(self, a, b):
        return a == b

    cpdef attrvalue(self, EventDispatcher obj):
        aval = self.get(obj)
        if aval is None:
            return ''
        if self.values:
            return self.values[aval]
        return aval

    property attrname:
        def __get__(self):
            if self._attrname:
                return self._attrname
            return self.name.replace("_", "-")

    property attrtype:
        def __get__(self):
            return self._attrtype

    property remote:
        def __get__(self):
            return self._remote

    property options:
        def __get__(self):
            return self.options

    property values:
        def __get__(self):
            return self.values


cdef class _NumericAttribute(NumericProperty):
    '''Html attribute class with a numeric value.

    :Parameters:
        `defaultvalue`: numeric, defaults to 0.0
        Specifies the default value of the attribute.
        `attrtype`: string, attribute type - 'style', 'class', 'element'
        `remote`: boolean, relevant for element attributes only, class and style attributes
        are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `options`: list of strings, other possible values which are accepted by the attribute instance.
        For example some numeric attributes can accept also string values like `inherit`
        `units`: valid CSS metric default units: 'px', 'mm', '%', etc. or empty
    '''
    
    def __cinit__(self):
        self.options = []
        self._remote = 0
        self._attrname = ''
        self._attrtype = ''

    def __init__(self, *largs, **kw):
        self._attrtype = kw.get('attrtype', '')
        self._attrname = kw.get('attrname', '')
        self._remote = kw.get('remote', 0)
        self.options = list(kw.get('options', []))
        kw['allownone'] = True
        super(_NumericAttribute, self).__init__(*largs, **kw)
    
    property remote:
        def __get__(self):
            return self._remote

    property attrname:
        def __get__(self):
            if self._attrname:
                return self._attrname
            return self.name.replace("_", "-")

    property attrtype:
        def __get__(self):
            return self._attrtype

    property options:
        def __get__(self):
            return self.options

    cpdef attrvalue(self, EventDispatcher obj):
        value = self.get(obj)
        if value is None:
            return ''
        if isinstance(value, string_types):
            return value
        else:
            fmt = self.get_units(obj)
            if fmt:
                return "{0}{1}".format(value, fmt)
        return "{0}".format(value)

    cdef check(self, EventDispatcher obj, value):
        if Property.check(self, obj, value):
            return True
        if type(value) not in (int, float, long, str):
            if value not in self.options:
                raise ValueError('%s.%s accept only int/float/long/str'
                                 ' and is set to an invalid option %r. '
                                 'Must be one of: %s' % (
                                     obj.__class__.__name__,
                                     self.name,
                                     value, self.options))

    cdef convert(self, EventDispatcher obj, x):
        if x is None:
            return x
        if x in self.options:
            return x
        tp = type(x)
        if tp is int or tp is float or tp is long:
            return x
        if tp is tuple or tp is list:
            if len(x) != 2:
                raise ValueError('%s.%s must have 2 components (got %r)' % (
                    obj.__class__.__name__,
                    self.name, x))
            return self.parse_list(obj, x[0], x[1])
        elif isinstance(x, string_types):
            return self.parse_str(obj, x)
        else:
            raise ValueError('%s.%s has an invalid format (got %r)' % (
                obj.__class__.__name__,
                self.name, x))


cdef class _BoundedNumericAttribute(BoundedNumericProperty):
    '''Html attribute class with a bounded numeric value.

    :Parameters:
        `defaultvalue`: numeric, defaults to 0.0
        Specifies the default value of the attribute.
        `attrtype`: string, attribute type - 'style', 'class', 'element'
        `remote`: boolean, relevant for element attributes only, class and style attributes
        are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `options`: list of strings, other possible values which are accepted by the attribute instance.
        For example some numeric attributes can accept also string values like `inherit`
        `min`: accepted minimum value
        `max`: accepted maximum value
        
    '''
    def __cinit__(self):
        self.options = []
        self._remote = 0
        self._attrname = ''
        self._attrtype = ''

    def __init__(self, *largs, **kw):
        self._attrtype = kw.get('attrtype', '')
        self._attrname = kw.get('attrname', '')
        self._remote = kw.get('remote', 0)
        self.options = list(kw.get('options', []))
        kw['allownone'] = True
        super(_BoundedNumericAttribute, self).__init__(*largs, **kw)
    
    property remote:
        def __get__(self):
            return self._remote

    property attrname:
        def __get__(self):
            if self._attrname:
                return self._attrname
            return self.name.replace("_", "-")

    property attrtype:
        def __get__(self):
            return self._attrtype

    property options:
        def __get__(self):
            return self.options

    cpdef attrvalue(self, EventDispatcher obj):
        value = self.get(obj)
        if value is None:
            return ''
        if isinstance(value, string_types):
            return value
        else:
            fmt = self.get_units(obj)
            if fmt:
                return "{0}{1}".format(value, fmt)
        return "{0}".format(value)

    cdef check(self, EventDispatcher obj, value):
        if type(value) not in (int, float, long):
            if value not in self.options:
                raise ValueError('%s.%s accept only int/float/long/str'
                                 ' and is set to an invalid option %r. '
                                 'Must be one of: %s' % (
                                     obj.__class__.__name__,
                                     self.name,
                                     value, self.options))
            return True
        else:
            return BoundedNumericProperty.check(self, obj, value)

    cdef convert(self, EventDispatcher obj, x):
        if x is None:
            return x
        if x in self.options:
            return x
        return BoundedNumericProperty.convert(self, obj, x)


cdef class _BooleanAttribute(BooleanProperty):
    '''Boolean Html attribute class which returns `attrname` as the
         return value when its true and empty string when its false.

    :Parameters:
        `defaultvalue`: boolean, defaults to false
        Specifies the default value of the attribute.
        `attrtype`: string, attribute type - 'style', 'class', 'element'
        `remote`: boolean, relevant for element attributes only, class and style attributes
        are always remote. If true the update event is sent to frontend when the attribute value is updated.
    '''
    
    def __cinit__(self):
        self._remote = 0
        self._attrname = ''
        self._attrtype = ''


    def __init__(self, *largs, **kw):
        self._attrtype = kw.get('attrtype', '')
        self._attrname = kw.get('attrname', '')
        self._remote = kw.get('remote', 0)
        super(_BooleanAttribute, self).__init__(*largs, **kw)

    property remote:
        def __get__(self):
            return self._remote

    property attrname:
        def __get__(self):
            if self._attrname:
                return self._attrname
            return self.name.replace("_", "-")

    property attrtype:
        def __get__(self):
            return self._attrtype

    cpdef attrvalue(self, EventDispatcher obj):
        value = self.get(obj)
        if value:
            return self.attrname
        return ''


cdef class _ListAttribute(ListProperty):
    '''List html attribute class which returns comma separated list of values as strings.

    :Parameters:
        `defaultvalue`: list, defaults to []
        Specifies the default value of the attribute.
        `attrtype`: string, attribute type - 'style', 'class', 'element'
        `remote`: boolean, relevant for element attributes only, class and style attributes
        are always remote. If true the update event is sent to frontend when the attribute value is updated.
    '''

    def __cinit__(self):
        self._remote = 0
        self._length = None
        self._attrname = ''
        self._attrtype = ''

    def __init__(self, *largs, **kw):
        self._attrtype = kw.get('attrtype', '')
        self._attrname = kw.get('attrname', '')
        self._remote = kw.get('remote', 0)
        self._length = kw.get('length', None)
        super(_ListAttribute, self).__init__(*largs, **kw)

    cdef check(self, EventDispatcher obj, value):
        if Property.check(self, obj, value):
            return True
        if self._length is not None and len(value) != self._length:
            raise ValueError('Length of input array is wrong, must be {}'.format(
                self._length))
        if type(value) is not ObservableList:
            raise ValueError('%s.%s accept only ObservableList' % (
                obj.__class__.__name__,
                self.name))

    property remote:
        def __get__(self):
            return self._remote

    property attrname:
        def __get__(self):
            if self._attrname:
                return self._attrname
            return self.name.replace("_", "-")

    property attrtype:
        def __get__(self):
            return self._attrtype

    cpdef attrvalue(self, EventDispatcher obj):
        value = self.get(obj)
        if not value:
            return ''
        return ", ".join([str(v) for v in value])


cdef class _LogicalAttribute(BooleanProperty):
    '''Logical html attribute class which returns 'true' or 'false' as string type and empty string when it is None.

    :Parameters:
        `defaultvalue`: boolean, defaults to False
        Specifies the default value of the attribute.
        `attrtype`: string, attribute type - 'style', 'class', 'element'
        `remote`: boolean, relevant for element attributes only, class and style attributes
        are always remote. If true the update event is sent to frontend when the attribute value is updated.
    '''

    def __cinit__(self):
        self.options = []
        self._remote = 0
        self._attrname = ''
        self._attrtype = ''

    def __init__(self, *largs, **kw):
        self._attrtype = kw.get('attrtype', '')
        self._attrname = kw.get('attrname', '')
        self._remote = kw.get('remote', 0)
        kw['allownone'] = True
        super(_LogicalAttribute, self).__init__(*largs, **kw)

    property remote:
        def __get__(self):
            return self._remote

    property attrname:

        def __get__(self):
            if self._attrname:
                return self._attrname
            return self.name.replace("_", "-")

    property attrtype:

        def __get__(self):
            return self._attrtype

    cpdef attrvalue(self, EventDispatcher obj):
        value = self.get(obj)
        if value is None:
            return ''
        if value:
            return 'true'
        return 'false'


cdef class _VariableListAttribute(VariableListProperty):
    '''A ListAttribute that allows you to work with a variable number of
    list items and to expand them to the desired list size.

    For example, style attributes like `margin` or `padding` can accept one numeric value
    which was applied equally to the left, top, right and bottom.
    Now padding or margin can be given one, two or four values, which are
    expanded into a length four list [left, top, right, bottom] and stored
    in the attribute.

    :Parameters:
        `defaultvalue`: list, defaults to []
        Specifies the default value of the attribute.
        `attrtype`: string, attribute type - 'style', 'class', 'element'
        `remote`: boolean, relevant for element attributes only, class and style attributes
        are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `options`: list of strings, other possible values which are accepted by the attribute instance
        For example some numeric attributes can accept also string values like `inherit`.
    '''
    
    def __cinit__(self):
        self._options = []
        self._remote = 0
        self._attrname = ''
        self._attrtype = ''

    def __init__(self, *largs, **kw):
        self._attrtype = kw.get('attrtype', '')
        self._attrname = kw.get('attrname', '')
        self._remote = kw.get('remote', 0)
        self._options = list(kw.get('options', []))
        kw['allownone'] = True
        super(_VariableListAttribute, self).__init__(*largs, **kw)

    property remote:
        def __get__(self):
            return self._remote

    cpdef attrvalue(self, EventDispatcher obj):
        value = self.get(obj)
        if value is None:
            return ''
        if isinstance(value, string_types):
            return value
        return " ".join([str(val) for val in value])

    cdef convert(self, EventDispatcher obj, x):
        if x is None:
            return x
        if x in self._options:
            return x
        tp = type(x)
        if isinstance(x, (list, tuple)):
            l = len(x)
            if l == 1:
                y = self._convert_numeric(obj, x[0])
                if self.length == 4:
                    return [y, y, y, y]
                elif self.length == 2:
                    return [y, y]
            elif l == 2:
                y = self._convert_numeric(obj, x[0])
                z = self._convert_numeric(obj, x[1])
                if self.length == 4:
                    return [y, z, y, z]
                elif self.length == 2:
                    return [y, z]
            elif l == 4:
                if self.length == 4:
                    return [self._convert_numeric(obj, y) for y in x]
                else:
                    err = '%s.%s must have 1 or 2 components (got %r)'
                    raise ValueError(err % (obj.__class__.__name__,
                                            self.name, x))
            else:
                if self.length == 4:
                    err = '%s.%s must have 1, 2 or 4 components (got %r)'
                elif self.length == 2:
                    err = '%s.%s must have 1 or 2 components (got %r)'
                raise ValueError(err % (obj.__class__.__name__, self.name, x))
        elif tp is int or tp is long or tp is float or isinstance(x, string_types):
            y = self._convert_numeric(obj, x)
            if self.length == 4:
                return [y, y, y, y]
            elif self.length == 2:
                return [y, y]
        else:
            raise ValueError('%s.%s has an invalid format (got %r)' % (
                obj.__class__.__name__,
                self.name, x))

    cdef _convert_numeric(self, EventDispatcher obj, x):
        tp = type(x)
        if tp is int or tp is float or tp is long:
            return x
        if isinstance(x, string_types):
            if x[-2:] in CSS_LENGTH_UNITS or x[-1:] == '%':
                return x
        else:
            raise ValueError('%s.%s has an invalid format (got %r)' % (
                obj.__class__.__name__,
                self.name, x))

    property attrname:
        def __get__(self):
            if self._attrname:
                return self._attrname
            return self.name.replace("_", "-")

    property attrtype:
        def __get__(self):
            return self._attrtype

    property options:
        def __get__(self):
            return self._options


cdef class _ColorAttribute(Property):
    '''A ListAttribute with 3 or 4 elements with RGB or RGBA color values.

    :Parameters:
        `defaultvalue`: list, defaults to None
        Specifies the default value of the attribute.
        `attrtype`: string, attribute type - 'style', 'class', 'element'
        `remote`: boolean, relevant for element attributes only, class and style attributes
        are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `options`: list of strings, other possible values which are accepted by the attribute instance
        For example some numeric attributes can accept also string values like `inherit`.
        'format': 'css' or empty string (default). If format is 'css', the color value will be converted to the proper
        css color attribute (with `rgb` or `rgba` prefix) 
    '''
    def __cinit__(self):
        self._options = []
        self._remote = 0
        self._attrname = ''
        self._format = ''
        self._attrtype = ''

    def __init__(self, defaultvalue=None, **kw):
        self._attrtype = kw.get('attrtype', '')
        self._attrname = kw.get('attrname', '')
        self._format = kw.get('format', '')
        self._remote = kw.get('remote', 0)
        self._options = list(kw.get('options', []))
        kw['allownone'] = True
        super(_ColorAttribute, self).__init__(defaultvalue, **kw)

    property remote:
        def __get__(self):
            return self._remote

    cdef convert(self, EventDispatcher obj, x):
        if x is None:
            return x
        if x in self._options:
            return x
        tp = type(x)
        if tp is tuple or tp is list:
            if len(x) != 3 and len(x) != 4:
                raise ValueError('{}.{} must have 3 or 4 components (got {!r})'.format(
                    obj.__class__.__name__, self.name, x))
            # if len(x) == 3:
            #    return list(x) + [1]
            return list(x)
        elif isinstance(x, string_types):
            return self.parse_str(obj, x)
        else:
            raise ValueError('{}.{} has an invalid format (got {!r})'.format(
                obj.__class__.__name__, self.name, x))

    cpdef attrvalue(self, EventDispatcher obj):
        value = self.get(obj)
        if value is None:
            return ''
        if isinstance(value, string_types):
            return value
        if len(value) == 3:
            if self._format == "css":
                return "rgb({0}, {1}, {2})".format(*value)
            return "{}, {}, {}".format(value[0] / 255, value[1] / 255, value[2] / 255)
        if self._format == "css":
            return "rgba({0}, {1}, {2}, {3})".format(*value)
        return "{}, {}, {}, {}".format(value[0] / 255, value[1] / 255, value[2] / 255, value[3])

    property attrname:
        def __get__(self):
            if self._attrname:
                return self._attrname
            return self.name.replace("_", "-")

    property attrtype:

        def __get__(self):
            return self._attrtype
        

    cdef list parse_str(self, EventDispatcher obj, value):
        return get_color_from_hex_255(value)


cdef class _GenericEventAttribute(BooleanProperty):
    '''An event attribute class like `onclick` for the button input element.
    Event attributes can be activated/deactivated  by setting them to True/False.

    :Parameters:
        `defaultvalue`: string, defaults to ''
        Specifies the default value of the attribute.
        `attrtype`: string, attribute type - 'style', 'class', 'element'
        `remote`: boolean, relevant for element attributes only, class and style attributes
        are always remote. If true the update event is sent to frontend when the attribute value is updated.
        `remdispatch`: boolean, if True, prevents dispatching of the remote event
        `event_handler`: callable, which returns handler javascript code.
        `event_args`: dictionary with event arguments
        'event_js': string with javascript code to be called when event is activated
    '''

    def __cinit__(self):
        self._attrtype = ''
        self._remote = 0
        self._remdispatch = True
        self._event_handler = None
        self._event_args = {}
        self._event_js = ''
        self._attrname = ''

    def __init__(self, defaultvalue=False,
                 eventhandler=None, prevent_default=False,
                 eventargs=None, eventjs='', dispatch=True, **kw):
        self._attrname = kw.get('attrname', '')
        self._attrtype = kw.get('attrtype', '')
        self._remote = kw.get('remote', 0)
        self._prevent_default = bool(prevent_default)
        self._remdispatch = bool(dispatch)
        self._event_handler = eventhandler
        self._event_args = eventargs or {}
        self._event_js = str(eventjs)
        kw['allownone'] = True
        super(_GenericEventAttribute, self).__init__(defaultvalue, **kw)

    cdef init_storage(self, EventDispatcher obj, PropertyStorage storage):
        BooleanProperty.init_storage(self, obj, storage)
        event = "on_remote_{}".format(self.attrname[2:])
        obj.register_remote_event_type(event)

    property remote:
        def __get__(self):
            return self._remote

    cpdef attrvalue(self, EventDispatcher obj):
        value = self.get(obj)
        if not value or self._event_handler is None:
            return ''
        hdlr = self._event_handler
        if isinstance(hdlr, string_types):
            return hdlr
        elif callable(hdlr):
            en = self.attrname
            return hdlr(obj, "on_remote_{}".format(en[2:]),
                        prevent_default=self._prevent_default,
                        event_args=self._event_args,
                        event_js=self._event_js, dispatch=self._remdispatch)
        else:
            raise AttributeError(
                "renderer must be of callable type or string, got {0}".format(type(hdlr)))

    property attrname:
        def __get__(self):
            if self._attrname:
                return self._attrname
            return self.name.replace("_", "-")

    property attrtype:
        def __get__(self):
            return self._attrtype
