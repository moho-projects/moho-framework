'''
Clock tests
===========
'''

import unittest
from moho.core.clock import Clock

counter = 0


def callback(dt):
    global counter
    counter += 1


class ClockTestCase(unittest.TestCase):

    def setUp(self):
        global counter
        counter = 0
        Clock._events = [[] for i in range(256)]

    def test_schedule_once(self):
        Clock.schedule_once(callback)
        Clock.tick()
        self.assertEqual(counter, 1)

    def test_schedule_once_twice(self):
        Clock.schedule_once(callback)
        Clock.schedule_once(callback)
        Clock.tick()
        self.assertEqual(counter, 2)

    def test_schedule_once_draw_after(self):
        Clock.schedule_once(callback, 0)
        Clock.tick_draw()
        self.assertEqual(counter, 0)
        Clock.tick()
        self.assertEqual(counter, 1)

    def test_schedule_once_draw_before(self):
        Clock.schedule_once(callback, -1)
        Clock.tick_draw()
        self.assertEqual(counter, 1)
        Clock.tick()
        self.assertEqual(counter, 1)

    def test_unschedule(self):
        Clock.schedule_once(callback)
        Clock.unschedule(callback)
        Clock.tick()
        self.assertEqual(counter, 0)

    def test_unschedule_after_tick(self):
        Clock.schedule_once(callback, 5.)
        Clock.tick()
        Clock.unschedule(callback)
        Clock.tick()
        self.assertEqual(counter, 0)

    def test_unschedule_draw(self):
        Clock.schedule_once(callback, 0)
        Clock.tick_draw()
        self.assertEqual(counter, 0)
        Clock.unschedule(callback)
        Clock.tick()
        self.assertEqual(counter, 0)
