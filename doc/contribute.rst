Contribute to Moho
---------------------------------

Thanks you for being interested in contributing to Moho!
   

Reporting a bug
++++++++++++++++++++
If you found anything wrong, like a crash, missing or invalid spelling in documentation or examples, please report the issue.

- Set the title of your issue
- Explain exactly what to do to reproduce the issue and paste the output with error
- Attach the failing code, if exists
- Validate the issue and you’re done!


Email
+++++++++++
You can also write us an email: escape.app.net@gmail.com 
